const express = require('express');
const router = express.Router();
const {checksAddValidators} = require('../helpers/validators');
const checksController = require('../controllers/checksController');

router.post('/', checksAddValidators, checksController.create);
router.get('/', checksController.get);
router.get('/:id', checksController.getOne);
router.delete('/:id', checksController.remove);
router.patch('/:id', checksController.update);

module.exports = router;
