const express = require('express');
const router = express.Router();
const {catsAddValidators} = require('../helpers/validators');
const catsController = require('../controllers/catsController');

router.post('/', catsAddValidators, catsController.create);
router.get('/', catsController.get);
router.delete('/:id', catsController.remove);

module.exports = router;
