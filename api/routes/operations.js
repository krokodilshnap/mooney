const express = require('express');
const router = express.Router();
const {operationValidators} = require('../helpers/validators');
const operationsController = require('../controllers/operationsController');

router.post('/', operationValidators, operationsController.create);
router.get('/chart', operationsController.getCharts);
router.get('/by/check/:id', operationsController.getByCheck);
router.get('/by/dates/:checkId', operationsController.getByDates);

module.exports = {router};
