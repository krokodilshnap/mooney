const catsRouter = require('./cats');
const checksRouter = require('./checks');
const operationsRouter = require('./operations');
const searchRouter = require('./search');
const usersRouter = require('./users');

function init(app) {
	app.use('/auth', usersRouter);
	app.use('/cats', catsRouter);
	app.use('/checks', checksRouter);
	app.use('/operations', operationsRouter.router);
	app.use('/search', searchRouter);
}

module.exports = {init};
