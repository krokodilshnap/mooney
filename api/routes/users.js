const passport = require('passport');
const {loginValidators, registerValidators, changeSettingsValidators, resetPasswordValidators} = require('../helpers/validators');
const express = require('express');
const router = express.Router();
const usersController = require('../controllers/usersController');
const config = require('../config');

router.post('/register', registerValidators, usersController.register);
router.post('/confirm', usersController.confirm);
router.post('/login', loginValidators, usersController.login);
router.get('/logout', usersController.logout);
router.get('/me', usersController.getMe);
router.post('/resetPassword', resetPasswordValidators, usersController.resetPassword);
router.patch('/', changeSettingsValidators, usersController.update);

router.get('/google',
	passport.authenticate('google', {
		scope: ['https://www.googleapis.com/auth/plus.login', 'email']
	})
);
router.get('/google/callback',
	passport.authenticate('google', {failureRedirect: config.APP_HOST + '/login'}),
	function(req, res) {
		res.redirect(config.APP_HOST + '/dashboard');
	}
);

router.get('/vkontakte', passport.authenticate('vkontakte', {scope: ['profile', 'email']}));
router.get('/vkontakte/callback',
	passport.authenticate('vkontakte', {
		successRedirect: config.APP_HOST + '/dashboard',
		failureRedirect: config.APP_HOST + '/login'
	})
);


module.exports = router;
