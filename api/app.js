const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const session = require('express-session');
const passport = require('passport');
const mongoose = require('mongoose');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);
const redisClient = redis.createClient();
const routes = require('./routes');
const port = 4000;

const app = express();

mongoose.connect('mongodb+srv://admin:6dFy16gOjQrkDqlN@cluster0.5i5dz.mongodb.net/mooney?retryWrites=true&w=majority', {
	useNewUrlParser: true, useUnifiedTopology: true,
});
const db = mongoose.connection;

app.use(session({
	store: new RedisStore({client: redisClient}),
	secret: 'secret',
	resave: false,
	saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());
require('./config/passportConfig')(passport);

app.use(cors({
	origin: 'http://localhost:8000',
	credentials: true,
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser('secret'));
app.use(express.static(path.join(__dirname, 'public')));

routes.init(app);

if (process.env.NODE_ENV !== 'development') {
	app.use(express.static(path.join(__dirname, 'build')));
	app.get('*', function(req, res) {
		res.sendFile(path.join(__dirname, 'build', 'index.html'));
	});
	app.use((req, res, next) => {
		if (!req.user) return res.status(401).send();
		next();
	});
}

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	res.status(err.status || 500);
	res.json({error: err});
});

db.once('open', () => {
	app.listen(port, () => {
		console.log(`App is running on port - ${port}. Successfully connected to db`);
	});
});
