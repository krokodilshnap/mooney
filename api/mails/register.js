function getRegisterTemplate(uid) {
	return `
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Mooney. Регистрация прошла успешно</title>
	</head>
	<body>
	<h1>Mooney</h1>
	<p>Регистрация прошла успешно. Необходимо подтвердить свой адрес электронной почты.</p>
	<a href="http://localhost:8000/dashboard?uid=${uid}" target="_blank">Подтвердить адрес</a>
	</body>
	</html>
	`;
}

module.exports = getRegisterTemplate;
