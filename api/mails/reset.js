function getResetTemplate(password) {
	return `
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Mooney. Смена пароля</title>
	</head>
	<body>
	<h1>Mooney</h1>
	<p>Похоже вы забыли пароль от вашей учетной записи Mooney.</p>
	<p>Новый пароль: ${password}</p>
	<small>Если вы не отправляли запрос на сброс пароля, пожалуйста обратитесь в нашу службу поддержки 
	<a href="mailto: info@mooney.ru">info@mooney.ru</a>
	</small>
	</body>
	</html>
	`;
}

module.exports = getResetTemplate;
