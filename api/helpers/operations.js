const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Operation = require('../models/Operations');

function aggregateOperations({income, month, check}) {
	return Operation.aggregate([
		{$match: {income, month: +month, check: ObjectId(check)}},
		{$lookup: {from: 'cats', foreignField: '_id', localField: 'cat', as: 'cat'}},
		{$unwind: '$cat'},
		{$group: {_id: '$cat', value: {$sum: '$value'}}}
	]);
}

function aggregateOperationsBalance({income, month, check}) {
	return Operation.aggregate([
		{$match: {income, month: +month, check: ObjectId(check)}},
		{$group: {_id: null, value: {$sum: '$value'}}}
	]);
}

function aggregateOperationsByDates(match, params) {
	if (params) {
		match = [
			{$match: {...match}},
			{$skip: +params.offset},
			{$limit: +params.limit}
		];
	} else {
		match = [{$match: {...match}}];
	}
	return Operation.aggregate([
		{$lookup: {from: 'cats', foreignField: '_id', localField: 'cat', as: 'cat'}},
		{$unwind: '$cat'},
		...match,
		{$group: {
			_id: {$dateToString: {format: '%Y-%m-%d', date: '$date'}},
			operations: {$push: {_id: '$_id', cat: '$cat', value: '$value'}}}},
		{$sort: {_id: -1}},
	]);
}

module.exports = {
	aggregateOperations,
	aggregateOperationsBalance,
	aggregateOperationsByDates
};
