const {body} = require('express-validator');
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const {check} = require('express-validator');
const emptyFieldMessage = 'Поле не может быть пустым';

function validateEmail() {
	return body('email')
		.not().isEmpty().withMessage(emptyFieldMessage)
		.isEmail().withMessage('Укажите корректный E-mail');
}

function validatePassword(field = 'password') {
	return body(field)
		.trim()
		.isLength({min: 6}).withMessage('Укажите пароль (мин. 6 сим.)');
}

function validateOperationSelect(field, message) {
	return body(field).not().isEmpty().withMessage(emptyFieldMessage).custom(async (value, {req}) => {
		return User.findOne({_id: req.user.id}).populate({path: field + 's', match: {_id: value}}).then((response) => {
			const results = response[field + 's'];
			if (!results && !results.length) {
				return Promise.reject(message);
			}
		});
	});
}

const loginValidators = [
	validateEmail()
		.custom((value) => {
			return User.findOne({email: value}).then((user) => {
				if (!user) {
					return Promise.reject('Пользователя с таким E-mail не существует');
				}
			});
		}),
	validatePassword().custom((value, {req}) => {
		const {email} = req.body;
		if (email) {
			return User.findOne({email}).then((user) => {
				return bcrypt.compare(value, user.password).then((result) => {
					if (result) return true;
					return Promise.reject('Неверный пароль');
				});
			});
		}

		return true;
	}),
];

const registerValidators = [
	validateEmail().custom((value) => {
		return User.findOne({email: value}).then((user) => {
			if (user) {
				return Promise.reject('Пользователь с таким E-mail уже существует');
			}
		});
	}),
	validatePassword().custom((value, {req}) => {
		if (value !== req.body.confirm) {
			throw new Error('Пароли не совпадают');
		}

		return true;
	}),
];

const changeSettingsValidators = [
	validatePassword().custom((value, {req}) => {
		const user = req.user;
		if (user) {
			return bcrypt.compare(value, user.password).then((result) => {
				if (result) return true;
				return Promise.reject('Неверный пароль');
			});
		}

		return true;
	}),
	validatePassword('new').custom((value, {req}) => {
		if (value !== req.body.confirm) {
			throw new Error('Пароли не совпадают');
		}

		return true;
	}),
];

const checksAddValidators = [
	body('name').not().isEmpty().withMessage(emptyFieldMessage),
];

const catsAddValidators = [
	check(['name', 'color']).not().isEmpty().withMessage(emptyFieldMessage),
];

const operationValidators = [
	body('value').not().isEmpty().withMessage(emptyFieldMessage),
	validateOperationSelect('check', 'Счет с данным id отсутствует у пользователя'),
	validateOperationSelect('cat', 'Категория с данным id отсутствует у пользователя'),
	body('date').not().isEmpty().withMessage(emptyFieldMessage).isISO8601().withMessage('Неверный формат даты'),
];

const resetPasswordValidators = [
	validateEmail()
		.custom((email) => {
			return User.find({email, password: {$exists: true}}).then((user) => {
				if (!user) {
					return Promise.reject('Пользователя с таким E-mail не существует');
				}
			});
		}),
]

module.exports = {
	loginValidators,
	registerValidators,
	changeSettingsValidators,
	checksAddValidators,
	catsAddValidators,
	operationValidators,
	resetPasswordValidators
};
