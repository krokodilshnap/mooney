const Check = require('../models/Checks');

async function createBasicCheck(user) {
	return await Check.create({
		name: 'Кошелек',
		balance: 0,
		userId: user.id
	});
}

module.exports = {createBasicCheck};
