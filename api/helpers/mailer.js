const nodemailer = require('nodemailer');

async function sendMail(user, template) {
	const transporter = nodemailer.createTransport({
		host: 'smtp.ethereal.email',
		port: 587,
		secure: false, // true for 465, false for other ports
		auth: {
			user: 'jaeden.schultz@ethereal.email', // generated ethereal user
			pass: '2tNXJaHR3gdJnfgdvq', // generated ethereal password
		},
	});
	const info = await transporter.sendMail({
		from: '"Mooney Registration" <info@mooney.com>', // sender address
		to: user.email, // list of receivers
		subject: 'Здравствуйте', // Subject line
		text: 'Регистрация прошла успешно', // plain text body
		html: template, // html body
	});

	console.log('Message sent: %s', info.messageId);
	console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
}

module.exports = sendMail;
