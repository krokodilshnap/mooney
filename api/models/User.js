const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const expireTime = 7 * 24 * 60 * 60;

const UserSchema = new Schema({
	email: String,
	password: String,
	googleId: String,
	vkontakteId: String,
	isConfirmed: {type: Boolean, default: false},
	expire_at: {type: Date, default: Date.now, expires: 7 * 24 * 60 * 60}
}, {toObject: {virtuals: true}, timestamps: {createdAt: 'created_at'}});

UserSchema.virtual('cats', {
	ref: 'Cat',
	foreignField: 'userId',
	localField: '_id',
});

UserSchema.virtual('checks', {
	ref: 'Check',
	foreignField: 'userId',
	localField: '_id',
});

module.exports = mongoose.model('User', UserSchema);
