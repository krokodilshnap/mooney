const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./User');

const CheckSchema = new Schema({
	name: String,
	balance: Number,
	userId: {type: mongoose.ObjectId, ref: User},
}, {
	toObject: {virtuals: true},
});

module.exports = mongoose.model('Check', CheckSchema);
