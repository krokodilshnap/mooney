const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./User');

const catsSchema = new Schema({
	name: String,
	color: String,
	income: Boolean,
	noCat: {type: Boolean, default: false},
	locked: {type: Boolean, default: false},
	userId: {type: mongoose.Mixed, ref: User},
});

module.exports = mongoose.model('Cat', catsSchema);
