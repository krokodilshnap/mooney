const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Cat = require('./Cats');
const Check = require('./Checks');
const User = require('./User');
const mongoosePaginate = require('mongoose-paginate-v2');

const OperationSchema = new Schema({
	value: Number,
	income: Boolean,
	month: {type: Number},
	cat: {type: mongoose.ObjectId, ref: Cat},
	check: {type: mongoose.ObjectId, ref: Check},
	date: Date,
	userId: {type: mongoose.ObjectId, ref: User}
});

OperationSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Operation', OperationSchema);
