const mongoose = require('mongoose');
const Operation = require('../models/Operations');
const {validationResult} = require('express-validator');
const Check = require('../models/Checks');
const ObjectId = mongoose.Types.ObjectId;
const {aggregateOperations, aggregateOperationsBalance, aggregateOperationsByDates} = require('../helpers/operations');

async function create(req, res) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(500).json({errors: errors.mapped()});
	}
	const {value, income, check: checkId, month, cat: catId, date} = req.body;
	try {
		const newOperation = new Operation({
			value, income, check: checkId, cat: catId, date, month, userId: req.user
		});
		let operation = await newOperation.save();
		const check = await Check.findOne({_id: checkId});
		check.balance = income ? check.balance + value : check.balance - value;
		await check.save();
		operation = await operation.populate('cat check').execPopulate();
		return res.send(operation);
	} catch (e) {
		return res.status(500).send(e);
	}
}

async function getCharts(req, res) {
	const {check, month} = req.query;
	const query = {
		month: +month,
		check: ObjectId(check)
	};
	const operationsCosts = await aggregateOperations({income: false, ...query});
	const operationsIncome = await aggregateOperations({income: true, ...query});
	let operationsIncomeBalance;
	let operationsCostsBalance;
	if (operationsIncome.length) {
		operationsIncomeBalance = await aggregateOperationsBalance({income: true, ...query});
	}
	if (operationsCosts.length) {
		operationsCostsBalance = await aggregateOperationsBalance({income: false, ...query});
	}
	const incomeBalance = operationsIncomeBalance ? operationsIncomeBalance[0].value : 0;
	const costsBalance = operationsCostsBalance ? operationsCostsBalance[0].value : 0;
	res.send({
		incomeBalance,
		costsBalance,
		balancePerPeriod: incomeBalance - costsBalance,
		income: operationsIncome,
		costs: operationsCosts
	});
}

async function getByCheck(req, res) {
	const {limit = 5, offset = 0, month} = req.query;
	try {
		const params = {
			populate: 'check cat',
			offset,
			limit,
			sort: {date: -1}
		};
		const query = {check: req.params.id, month};
		const operationsIncome = await Operation.paginate({...query, income: true}, params);
		const operationsCosts = await Operation.paginate({...query, income: false}, params);
		res.send({
			income: operationsIncome,
			costs: operationsCosts
		});
	} catch (e) {
		return res.status(500).send(e);
	}
}

async function getByDates(req, res) {
	const {month} = req.query;
	const {checkId} = req.params;
	const query = {
		month: +month,
		check: ObjectId(checkId)
	};
	try {
		const operationsCosts = await aggregateOperationsByDates({income: false, ...query});
		const operationsIncome = await aggregateOperationsByDates({income: true, ...query});
		res.send({
			income: operationsIncome,
			costs: operationsCosts
		});
	} catch (e) {
		return res.status(500).send(e);
	}
}

module.exports = {create, getCharts, getByCheck, getByDates};
