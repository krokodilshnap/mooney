const {validationResult} = require('express-validator');
const Cat = require('../models/Cats');
const Operation = require('../models/Operations');

async function create(req, res) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) return res.status(500).json({errors: errors.mapped()});

	let {name, color, income = 'false'} = req.body;
	income = income === 'true';
	try {
		const cat = await Cat.create({name, color, income, userId: req.user.id});
		return res.status(201).send(cat);
	} catch (e) {
		return res.status(500).send();
	}
}

async function get(req, res) {
	try {
		const cats = await Cat.find({userId: req.user.id});
		const catsForAll = await Cat.find().or([{noCat: true}, {userId: 'all'}]);
		return res.send([...catsForAll, ...cats]);
	} catch (e) {
		return res.status(500).send();
	}
}

async function remove(req, res) {
	try {
		await Cat.deleteOne({_id: req.params.id});
		const noCat = await Cat.findOne({noCat: true});
		await Operation.updateMany({cat: req.params.id}, {cat: noCat._id});
		return res.end();
	} catch (e) {
		return res.status(500).send();
	}
}

module.exports = {create, get, remove};
