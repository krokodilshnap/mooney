const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const User = require('../models/User');
const getResetTemplate = require('../mails/reset');
const getRegisterTemplate = require('../mails/register');
const sendMail = require('../helpers/mailer');
const {createBasicCheck} = require('../helpers/checks');
const passport = require('passport');
const generator = require('generate-password');

async function register(req, res) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(500).json({errors: errors.array()});
	}

	const {email, password} = req.body;
	const hashedPassword = await bcrypt.hash(password, 10);
	const newUser = new User({
		email, password: hashedPassword,
	});
	const user = await newUser.save();
	sendMail(user, getRegisterTemplate(user._id)).catch(console.error);
	await createBasicCheck(user);
	res.send(user);
}

async function confirm(req, res) {
	const {uid} = req.body;
	try {
		await User.findByIdAndUpdate(uid, {isConfirmed: true, $unset: {expire_at: 1}});
		return res.end();
	} catch (e) {
		return res.status(500).send({errors: ['Произошла ошибка. Попробуйте позже']});
	}
}

function login(req, res, next) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(500).json({errors: errors.array()});
	}
	passport.authenticate('local', (err, user) => {
		if (err) throw err;
		req.login(user, (err) => {
			if (err) throw err;
			res.send(user);
		});
	})(req, res, next);
}

function logout(req, res) {
	req.logout();
	res.end();
}

function getMe(req, res) {
	if (req.user) return res.send(req.user);
	return res.status(401).send();
}

async function update(req, res) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(500).json({errors: errors.array()});
	}

	const hashedPassword = await bcrypt.hash(req.body.new, 10);
	await User.findOneAndUpdate({email: req.user.email}, {password: hashedPassword});

	return res.status(200).send();
}

async function resetPassword(req, res) {
	const {email} = req.body;
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(500).json({errors: errors.array()});
	}
	try {
		const password = generator.generate({
			length: 10,
			numbers: true
		});
		const hashedPassword = await bcrypt.hash(password, 10);
		const user = await User.findOneAndUpdate({email, password: {$exists: true}}, {
			password: hashedPassword
		});
		sendMail(user, getResetTemplate(password)).catch(console.error);
	} catch (e) {
		return res.status(500).send();
	}
}

module.exports = {
	register,
	confirm,
	login,
	logout,
	getMe,
	update,
	resetPassword
};
