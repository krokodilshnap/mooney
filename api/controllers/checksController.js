const Check = require('../models/Checks');
const {validationResult} = require('express-validator');
const User = require('../models/User');
const Operation = require('../models/Operations');

async function create(req, res) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(500).json({errors: errors.mapped()});
	} else {
		const {name, balance} = req.body;
		const newCheck = new Check({name, balance, userId: req.user.id});
		const check = await newCheck.save();
		return res.send(check);
	}
}

async function get(req, res) {
	try {
		const checksResponse = await User.findById(req.user.id).populate('checks');
		return res.send(checksResponse.checks);
	} catch (e) {
		return res.status(500).send(e);
	}
}

async function getOne(req, res) {
	try {
		const check = await Check.findOne({_id: req.params.id});
		res.send(check);
	} catch (e) {
		return res.status(500).send(e);
	}
}

async function remove(req, res) {
	const {id} = req.params;
	try {
		Check.deleteOne({_id: id}, () => res.end());
		await Operation.deleteMany({check: id});
	} catch (e) {
		return res.status(500).send(e);
	}
}

async function update(req, res) {
	const {id} = req.params;
	const values = req.body;
	try {
		Check.findById(id, (err, doc) => {
			if (err) throw err;
			const hasIncomeProperty = Object.prototype.hasOwnProperty.call(values, 'income');
			for (const key in values) {
				if (Object.prototype.hasOwnProperty.call(values, key)) {
					if (hasIncomeProperty) {
						values.balance = values.income ? doc.balance + values.balance : doc.balance - values.balance;
					}
					if (!key.startsWith('_')) {
						doc[key] = values[key];
					}
				}
			}
			doc.save();
			return res.send(doc);
		});
	} catch (e) {
		return res.status(500).send(e);
	}
}

module.exports = {create, get, getOne, remove, update};
