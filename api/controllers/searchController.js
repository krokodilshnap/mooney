const {aggregateOperationsByDates} = require('../helpers/operations');
async function get(req, res) {
	const {query, limit, offset} = req.query;
	const params = {
		limit, offset
	};
	try {
		const operations = await aggregateOperationsByDates({
			'cat.name': query ? {'$regex': query, '$options': 'i'} : null
		}, params);
		res.send(operations);
	} catch (e) {
		return res.status(500).send(e);
	}
}

module.exports = {get};
