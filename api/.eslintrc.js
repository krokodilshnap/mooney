module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
  },
  'extends': [
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
  },
  'rules': {
    'indent': ['error', 'tab'],
    'no-tabs': 'off',
    'new-cap': 'off',
    'max-len': ['error', 140],
    'require-jsdoc': 'off',
    'comma-dangle': 'off'
  },
};
