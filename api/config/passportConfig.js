const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const VKontakteStrategy = require('passport-vkontakte').Strategy;
const {createBasicCheck} = require('../helpers/checks');

// todo доделать функцию чтобы в вк не было ошибки
function findOrCreate(idKey) {
	return async function(...args) {
		console.log(args);
		const profile = args[args.length - 2];
		const done = args[args.length - 1];
		const currentUser = await User.findOne({[idKey]: profile.id});
		if (currentUser) {
			done(null, currentUser);
		} else {
			const newUser = await new User({
				[idKey]: profile.id,
				email: profile.emails[0].value,
				isConfirmed: true,
			}).save();
			await newUser.update({$unset: {expire_at: 1}});
			await createBasicCheck(newUser);
			done(null, newUser);
		}
	};
}


module.exports = function(passport) {
	passport.use(new LocalStrategy({usernameField: 'email', passwordField: 'password'}, (username, password, done) => {
		User.findOne({email: username}, (err, user) => {
			if (err) throw err;
			return done(null, user);
		});
	}));

	passport.use(new GoogleStrategy({
		clientID: '16693361413-b7u4tvu5f7rcb54im2jockup8f36ikaj.apps.googleusercontent.com',
		clientSecret: 'kLRXzqg1fykubKm78ba8eDZ5',
		callbackURL: 'http://localhost:4000/auth/google/callback'
	},
	async function(accessToken, refreshToken, profile, done) {
		const currentUser = await User.findOne({googleId: profile.id});
		if (currentUser) {
			done(null, currentUser);
		} else {
			const newUser = await new User({
				googleId: profile.id,
				email: profile.emails[0].value,
				isConfirmed: true,
			}).save();
			await newUser.update({$unset: {expire_at: 1}});
			await createBasicCheck(newUser);
			done(null, newUser);
		}
	}));

	passport.use(new VKontakteStrategy(
		{
			clientID: '7630331',
			clientSecret: 'gDuJgh8DVLselZsl7Sk8',
			callbackURL: 'http://localhost:4000/auth/vkontakte/callback'
		},
		async function(accessToken, refreshToken, params, profile, done) {
			const currentUser = await User.findOne({vkontakteId: profile.id});
			if (currentUser) {
				done(null, currentUser);
			} else {
				const newUser = await new User({
					vkontakteId: profile.id,
					email: profile.emails[0].value,
					isConfirmed: true,
				}).save();
				await newUser.update({$unset: {expire_at: 1}});
				await createBasicCheck(newUser);
				done(null, newUser);
			}
		}
	));

	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});
};
