import React , {useEffect} from 'react';
import Header from '../../components/Header/Header';
import Sidebar from '../../components/Sidebar/Sidebar';
import classes from './main.module.scss';
import useChecks from '../../hooks/useChecks';
import {useCats} from '../../hooks/useCats';
import PropTypes from 'prop-types';
import {useToaster} from '../../hooks/useToaster';
import {useSelector} from 'react-redux';

const Main = ({children}) => {
	const {showToaster} = useToaster();
	const {user} = useSelector(state => state.auth);
	useChecks();
	useCats();

	useEffect(() => {
		let toaster;
		if (user && !user.isConfirmed) {
			toaster = showToaster(<p>Письмо с инструкцией отправлено на <strong>{user.email}</strong>.
				Неподтвержденный аккаунт будет удален через 7 дней.</p>, 'info', {
				heading: 'Подтвердите свой аккаунт',
				position: 'bottom-right',
				hideAfter: 0,
				renderIcon: () => null
			});
		}

		return () => {
			if (toaster) {
				toaster.hide();
			}
		};
	}, [user]);

	return (
		<main className="container">
			<Header/>
			<section className={classes.main}>
				<Sidebar/>
				<section className={[classes.mainContent, classes.contentSection].join(' ')}>
					{children}
				</section>
			</section>

		</main>
	);
};

Main.propTypes = {
	children: PropTypes.element.isRequired
};

export default Main;
