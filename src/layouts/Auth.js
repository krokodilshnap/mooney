import React from 'react';
import PropTypes from 'prop-types';

const Auth = ({children}) => {
	return (
		<>
			{children}
		</>
	);
};

Auth.propTypes = {
	children: PropTypes.element.isRequired
};

export default Auth;
