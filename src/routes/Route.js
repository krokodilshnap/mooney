import React, {useEffect, useState} from 'react';
import {Redirect, Route} from 'react-router-dom';
import Main from '../layouts/Main/Main';
import Auth from '../layouts/Auth';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {autoLogin} from '../store/actions/auth';
import PropTypes from 'prop-types';

const RouteWrapper = ({component: Component, notPrivate, ...rest}) => {
	const user = useSelector((state) => state.auth.user);
	const showBackdrop = useSelector((state) => state.global.showBackdrop);
	const dispatch = useDispatch();
	const history = useHistory();
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		const getUser = async () => {
			await dispatch(autoLogin());
			setLoaded(true);
		};

		getUser();
		// eslint-disable-next-line
	}, []);
	
	if (loaded) {
		const path = history.location.pathname;
		if (!notPrivate && !user) return <Redirect to="/" />;
		if (notPrivate && user) return <Redirect to={path !== '/' ? path : '/dashboard'}/>;

		const Layout = user ? Main : Auth;

		return (
			<>
				{showBackdrop ? <div className="backdrop"/> : null}
				<Route {...rest} render={(props) => <Layout><Component {...props}/></Layout>} />
			</>
		);
	} else {
		return null;
	}
};

RouteWrapper.propTypes = {
	component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
	notPrivate: PropTypes.bool
};

export default RouteWrapper;
