import React from 'react';
import {Switch} from 'react-router-dom';
import Dashboard from '../pages/dashboard/Dashboard';
import Login from '../pages/login/Login';
import Route from './Route';
import Categories from '../pages/cats/Categories';
import Settings from '../pages/profile/settings/Settings';
import Checks from '../pages/checks/Checks';
import Search from '../pages/search/Search';

const Routes = (props) => {
	return (
		<Switch>
			<Route path={'/'} exact component={Login} notPrivate />
			<Route path={'/dashboard'} component={Dashboard} />
			<Route path={'/search'} component={Search} />
			<Route path={'/cats'} component={Categories} />
			<Route path={'/checks'} component={Checks} />
			<Route path={'/settings'} component={Settings} />
		</Switch>
	);
};

export default Routes;
