import React from 'react';
import classes from './register.module.scss';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';
import {instance as axios} from '../../common/axios';
import {auth} from '../../store/actions/auth';
import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {useToaster} from '../../hooks/useToaster';

const RegisterForm = (props) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const {showToaster} = useToaster();

	async function submitRegister(values, {resetForm}) {
		try {
			await axios.post('/auth/register', values);
			await dispatch(auth(values.password, values.email));
			resetForm({values: ''});
			history.push('/dashboard');
		} catch (e) {
			showToaster(e.response.data.errors[0].msg, 'error');
		}
	}

	return (
		<div className={classes.form}>
			<Formik
				initialValues={{email: '', password: '', confirm: ''}}
				validationSchema={Yup.object({
					password: Yup.string()
						.min(6, 'Пароль должен быть не менее 6 символов')
						.required('Введите пароль'),
					email: Yup.string()
						.email('Некоректный E-mail')
						.required('Введите E-mail'),
					confirm: Yup.string()
						.oneOf([Yup.ref('password'), null], 'Пароли не совпадают')
				})}
				onSubmit={submitRegister}
			>

				<Form>
					<div className={classes.formGroup}>
						<label htmlFor="email">E-mail:</label>
						<Field name="email" type="email" placeholder="Введите ваш E-mail" />
						<ErrorMessage name="email" component="div" className={classes.formGroupError}/>
					</div>

					<div className={classes.formGroup}>
						<label htmlFor="password">Пароль:</label>
						<Field name="password" type="password" placeholder="Введите ваш пароль"/>
						<ErrorMessage name="password" component="div" className={classes.formGroupError}/>
					</div>

					<div className={classes.formGroup}>
						<label htmlFor="confirm">Повторите пароль:</label>
						<Field name="confirm" type="password" placeholder="Повторите пароль"/>
						<ErrorMessage name="confirm" component="div" className={classes.formGroupError}/>
					</div>

					<div className={classes.politics}>
                        Нажимая кнопку "Регистрация", вы принимаете
						<a href="/politics" target="_blank"> условия пользовательского соглашения</a>
					</div>

					<div className={classes.loginFormControls}>
						<button type="submit" className={[classes.button, classes.buttonBlue].join(' ')}>Регистрация</button>
					</div>

				</Form>

			</Formik>
		</div>
	);
};

export default RegisterForm;
