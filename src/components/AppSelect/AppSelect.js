import React from 'react';
import Select from 'react-select';
import {customStyles, DropdownIndicator, NoOptionsMessage} from './styles';
import PropTypes, {shape} from 'prop-types';

const AppSelect = (props) => {
	return (
		<Select
			{...props}
			defaultValue={props.options[0]}
			components={{DropdownIndicator, NoOptionsMessage}}
			styles={customStyles}
			onChange={props.onChange}/>
	);
};

AppSelect.propTypes = {
	options: PropTypes.arrayOf(shape({
		value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		label: PropTypes.string
	})).isRequired,
	getOptionLabel: PropTypes.func,
	getOptionValue: PropTypes.func,
	onChange: PropTypes.func
};

export default AppSelect;
