import {components} from 'react-select';
import Chevron from '../../img/chevron.svg';
import React from 'react';

export const customStyles = {
	option: (provided, {isSelected}) => ({
		...provided,
		color: isSelected ? 'white' : '#699FD5',
		fontWeight: 500,
		fontSize: 12
	}),
	control: (provided, state) => ({
		...provided,
		background: 'linear-gradient(134.17deg, #EEF0F5 4.98%, #E6E9EF 94.88%)',
		boxShadow: '-5px -5px 10px rgba(255, 255, 255, 0.8), 3px 3px 12px rgba(166, 180, 200, 0.7)',
		borderRadius: '5px',
		border: 0,
		minHeight: 24,
		cursor: 'pointer'
	}),
	singleValue: (provided, state) => ({
		...provided,
		color: '#699FD5',
		fontSize: 12,
		fontWeight: 500
	}),
	indicatorSeparator: (provided, state) => ({
		...provided,
		display: 'none'
	}),
	dropdownIndicator: (provided, state) => ({
		...provided,
		padding: '0 5px'
	}),
	menu: (provided, state) => ({
		...provided,
		background: 'linear-gradient(134.17deg, #EEF0F5 4.98%, #E6E9EF 94.88%)',
		boxShadow: '-5px -5px 10px rgba(255, 255, 255, 0.8), 3px 3px 12px rgba(166, 180, 200, 0.7)',
		borderRadius: '5px',
		zIndex: '1000'
	}),
	noOptionsMessage: (provided, state) => ({
		...provided,
		color: '#699FD5'
	})
};

export const DropdownIndicator = (props) => {
	return (
		<components.DropdownIndicator {...props}>
			{/* eslint-disable-next-line react/prop-types */}
			<img src={Chevron} alt="chevron" className={props.selectProps.menuIsOpen ? 'rotate-180' : null}/>
		</components.DropdownIndicator>
	);
};

export const NoOptionsMessage = (props) => {
	return (
		<components.NoOptionsMessage {...props}>
			<span>Нет данных</span>
		</components.NoOptionsMessage>
	);
};
