import React, {useState} from 'react';
import Logo from '../Logo/Logo';
import classes from './header.module.scss';
import Dropdown from '../Dropdown/Dropdown';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {logoutUser} from '../../store/actions/auth';

const Header = (props) => {
	const user = useSelector((state) => state.auth.user);
	const dispatch = useDispatch();
	const history = useHistory();
	const [options] = useState([
		{
			title: 'Настройки',
			value: 'settings',
			link: true
		}
	]);

	const logout = async () => {
		await dispatch(logoutUser());
		history.push('/');
	};

	return (
		<header className={classes.mainHeader}>
			<Logo />
			<Dropdown options={options} title={user.email} >
				<div className={classes.headerLogout} onClick={logout}>Выход</div>
			</Dropdown>
		</header>
	);
};

export default Header;
