import React, {useState} from 'react';
import classes from './check-item.module.scss';
import {ReactComponent as PenIcon} from '../../img/pen.svg';
import {ReactComponent as DeleteIcon} from '../../img/plus.svg';
import Popover from 'react-awesome-popover';
import FormConfirm from '../FormConfirm/FormConfirm';
import {useDispatch} from 'react-redux';
import {deleteCheck} from '../../store/actions/checks';
import FormAddCheck from '../FormAddCheck/FormAddCheck';
import PropTypes from 'prop-types';

const CheckItem = ({info}) => {
	const dispatch = useDispatch();
	const [deleteVisible, setDeleteVisible] = useState(false);
	const [editVisible, setEditVisible] = useState(false);
	const deleteCheckHandler = () => {
		dispatch(deleteCheck(info._id));
		setDeleteVisible(false);
	};

	return (
		<div className={classes.checkItem}>
			<span className={classes.checkItemTitle}>{info.name}</span>
			<span className={classes.checkItemBalance}>{info.balance ? info.balance : 0} ₽</span>
			<div className={classes.checkItemControls}>
				<Popover
					open={editVisible}
					onOpen={() => setEditVisible(true)}
					onClose={() => setEditVisible(false)}
					placement="right-start"
					arrow={false}
					className={classes.popper}>
					<span className={[classes.checkItemButton, classes.checkItemButtonEdit].join(' ')}>
						<PenIcon/>
					</span>
					<div className="popper-content popper-content__thin" style={{marginLeft: 20}}>
						<FormAddCheck isEdit onEdit={() => setEditVisible(false)} defaultValues={info} />
					</div>
				</Popover>
				<Popover
					open={deleteVisible}
					onOpen={() => setDeleteVisible(true)}
					onClose={() => setDeleteVisible(false)}
					placement="right-start"
					arrow={false}
					className={classes.popper}>
					<span className={classes.checkItemButton}>
						<DeleteIcon className={classes.rotate45}/>
					</span>
					<div className="popper-content popper-content__thin" style={{marginLeft: 20}}>
						<FormConfirm onConfirm={deleteCheckHandler} onDecline={() => setDeleteVisible(false)}>
							<p>Вы уверены?</p>
							<p style={{color: 'red'}}>Все операции по счету будут удалены!</p>
						</FormConfirm>
					</div>
				</Popover>
			</div>
		</div>
	);
};

CheckItem.propTypes = {
	info: PropTypes.shape({
		_id: PropTypes.string,
		name: PropTypes.string,
		balance: PropTypes.number
	})
};

export default CheckItem;
