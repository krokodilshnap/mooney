import React from 'react';
import {ReactComponent as SearchIcon} from '../../img/search.svg';
import classes from './search.module.scss';
import {useHistory} from 'react-router-dom';

const Search = (props) => {
	const history = useHistory();
	function onSearch(e) {
		if (e.key === 'Enter') {
			const value = e.target.value;
			history.push(`/search?query=${value}`);
		}
	}
	return (
		<label className={classes.search}>
			<SearchIcon/>
			<input type="text" placeholder="Поиск" onKeyUp={onSearch}/>
		</label>
	);
};

export default Search;
