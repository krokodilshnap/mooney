import React, {useEffect, useRef, useState} from 'react';
import classes from './buttons.module.scss';
import PropTypes from 'prop-types';

const SlideButtons = ({buttons, onChange}) => {
	const [trackWidth, setTrackWidth] = useState(200);
	const [trackLeft, setTrackLeft] = useState('0px');
	const activeButton = useRef();

	useEffect(() => {
		setTrackWidth(activeButton.current.offsetWidth);
		setTrackLeft(activeButton.current.offsetLeft + 'px');
	}, [buttons]);

	return (
		<div className={classes.slideButtons}>
			{buttons.map((button, index) => (
				<button
					ref={button.active ? activeButton : null}
					className={button.active ? classes.active : ''}
					key={index}
					onClick={() => onChange(index)}>
					{button.title}
				</button>
			))}

			<div className={classes.slideTrack} style={{width: trackWidth, left: trackLeft}}/>
		</div>
	);
};

SlideButtons.propTypes = {
	buttons: PropTypes.arrayOf(PropTypes.shape({
		active: PropTypes.bool.isRequired,
		title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
	})).isRequired,
	onChange: PropTypes.func
};

export default SlideButtons;
