export function getOption(operations, income) {
	let data = [];
	if (operations.length) {
		data = operations.map(operation => {
			return {value: operation.value, name: operation._id.name, itemStyle: {color: operation._id.color}};
		});
	}

	return {
		tooltip: {
			trigger: 'item',
			formatter: '<strong>{b}</strong><br> {c} ₽ ({d}%)',
			backgroundColor: 'rgba(0,0,0,0.8)',
			padding: [5, 10],
			textStyle: {
				fontFamily: 'Ubuntu',
			}
		},
		grid: {
			left: '2%',
			top: '2%',
			right: '2%',
			bottom: '6%',
			show: false,
			width: '100%',
			height: '100%',
		},
		series: [
			{
				name: income ? 'Доходы' : 'Расходы',
				type: 'pie',
				hoverAnimation: false,
				radius: ['70%', '100%'],
				label: {
					show: false,
					position: 'center'
				},
				labelLine: {
					show: false
				},
				data
			}
		]
	};
}
