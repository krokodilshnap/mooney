import React , {useCallback , useMemo} from 'react';
import classes from './chart.module.scss';
import ReactEchartsCore from 'echarts-for-react/lib/core';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/pie';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import PropTypes from 'prop-types';
import {getOption} from './getOption';

const ChartPie = ({operations, balance, income}) => {
	const getOptions = useCallback(() => {
		return getOption(operations, income);
	}, [operations, income]);
	// function getOptions(operations, income) {
	// 	return getOption(operations, income);
	// }
	return (
		<div className={classes.chartWrapper}>
			{balance !== undefined && <div className={classes.chartWrapperCenter}>
				{income ? '+' : '-'} {balance} ₽
			</div>}
			<ReactEchartsCore
				echarts={echarts}
				option={getOptions()}
				notMerge={true}
				lazyUpdate={true}
				theme={'theme_name'}/>
		</div>
	);
};

ChartPie.propTypes = {
	income: PropTypes.bool,
	balance: PropTypes.number.isRequired,
	operations: PropTypes.arrayOf(PropTypes.shape({
		_id: PropTypes.object,
		value: PropTypes.number
	})).isRequired
};

export default ChartPie;
