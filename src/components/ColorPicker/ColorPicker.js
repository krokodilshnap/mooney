import React, {useState} from 'react';
import classes from './color-picker.module.scss';
import PropTypes from 'prop-types';

const ColorPicker = ({colors, onChange}) => {
	const [activeIndex, setActiveIndex] = useState();
	const handleChangeColor = (e, color, index) => {
		setActiveIndex(index);
		onChange(color, e);
	};
	return (
		<div className={classes.colorPicker}>
			{colors.map((color, index) => (
				<span
					style={activeIndex !== undefined && activeIndex === index ?
						{backgroundColor: 'transparent', border: `2px solid ${color}`} :
						{backgroundColor: color}} key={color} onClick={(e) => handleChangeColor(e, color, index)} />
			))}
		</div>
	);
};

ColorPicker.propTypes = {
	colors: PropTypes.arrayOf(PropTypes.string).isRequired,
	onChange: PropTypes.func
};

export default ColorPicker;
