import React , {useState} from 'react';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';
import classes from './login-form.module.scss';
import google from '../../img/google.svg';
import vk from '../../img/vk.svg';
import {useDispatch} from 'react-redux';
import {auth} from '../../store/actions/auth';
import {useHistory} from 'react-router-dom';
import Popover from 'react-awesome-popover';
import FormResetPassword from '../FormResetPassword/FormResetPassword';
import {useToaster} from '../../hooks/useToaster';


const LoginForm = (props) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [resetState, setResetState] = useState(false);
	const {showToaster} = useToaster();

	async function formSubmit({password, email}) {
		try {
			await dispatch(auth(password, email));
			history.push('/dashboard');
		} catch (e) {
			console.log(e.response);
			showToaster(e.response.data.errors[0].msg, 'error');
		}
	}

	return (
		<div className={classes.form}>
			<Formik
				initialValues={{email: '', password: ''}}
				validationSchema={Yup.object({
					password: Yup.string()
						.min(6, 'Пароль должен быть не менее 6 символов')
						.required('Введите пароль'),
					email: Yup.string()
						.email('Некоректный E-mail')
						.required('Введите E-mail'),
				})}
				onSubmit={formSubmit}
			>
				<Form>
					<div className={classes.formGroup}>
						<label htmlFor="email">E-mail:</label>
						<Field name="email" type="email" placeholder="Введите ваш E-mail" />
						<ErrorMessage name="email" component="div" className={classes.formGroupError}/>
					</div>

					<div className={classes.formGroup}>
						<label htmlFor="password">Пароль:</label>
						<Field name="password" type="password" placeholder="Введите ваш пароль"/>
						<ErrorMessage name="password" component="div" className={classes.formGroupError}/>
					</div>

					<div className={classes.loginFormControls}>
						<button type="submit" className={[classes.button, classes.buttonBlue].join(' ')}>Вход</button>
						<Popover
							open={resetState}
							onOpen={() => setResetState(true)}
							onClose={() => setResetState(false)}
							placement="left-start"
							arrow={false}
							className={classes.popper}>
							<div className={classes.forgot}>Забыли пароль?</div>
							<div className="popper-content" style={{marginLeft: 20}}>
								<FormResetPassword onSubmit={() => setResetState(false)}/>
							</div>
						</Popover>
					</div>

					<div className={classes.separator}>
                        или
						<div />
					</div>

					<section className={classes.socials}>
						<a href="http://localhost:4000/auth/vkontakte">
							<img src={vk} alt="Vk"/>
						</a>
						<a href="http://localhost:4000/auth/google">
							<img src={google} alt="Google"/>
						</a>
					</section>

				</Form>

			</Formik>
		</div>

	);
};

export default LoginForm;
