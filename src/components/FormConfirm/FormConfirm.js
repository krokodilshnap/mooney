import React from 'react';
import classes from './form-confirm.module.scss';
import PropTypes from 'prop-types';

const FormConfirm = ({onConfirm, onDecline, children}) => {
	return (
		<div className={classes.formConfirm}>
			{children ? children : <p>Вы уверены?</p>}
			<div className={classes.formConfirmButtons}>
				<button className={[classes.button, classes.buttonBlue].join(' ')} onClick={onConfirm}>
					Да
				</button>
				<button className={classes.button} onClick={onDecline}>
					Нет
				</button>
			</div>
		</div>
	);
};

FormConfirm.propTypes = {
	onConfirm: PropTypes.func.isRequired,
	onDecline: PropTypes.func.isRequired,
	children: PropTypes.element
};

export default FormConfirm;
