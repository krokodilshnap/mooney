import React from 'react';
import classes from './loader.module.scss';

const Loader = props => {
	return (
		<div className={classes.ellipsisWrapper}>
			<div className={classes.ellipsis}>
				<div/>
				<div/>
				<div/>
				<div/>
			</div>
		</div>

	);
};

export default Loader;