import React from 'react';
import classes from './logo.module.scss';
import PropTypes from 'prop-types';

const Logo = (props) => {
	const classNames = [classes.logo];

	if (props.big) {
		classNames.push(classes.big);
	}

	return (
		<div className={classNames.join(' ')}>
			<h1>Mooney</h1>
		</div>
	);
};

Logo.propTypes = {
	big: PropTypes.bool
};

export default Logo;
