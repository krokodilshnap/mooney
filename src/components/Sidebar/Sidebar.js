import React, {useState} from 'react';
import classes from './sidebar.module.scss';
import Search from '../Search/Search';
import Menu from '../Menu/Menu';
import FormAdding from '../FormAdding/FormAdding';
import Popover from 'react-awesome-popover';

const Sidebar = (props) => {
	const [addState, setAddState] = useState(false);
	return (
		<aside className={classes.sidebar}>
			<div className={classes['mb-16']}>
				<Popover
					open={addState}
					onOpen={() => setAddState(true)}
					onClose={() => setAddState(false)}
					placement="right-start"
					arrow={false}
					className={classes.popper}>
					<button className={[classes.button, classes.buttonBlue].join(' ')}>Добавить расход / доход</button>
					<div className="popper-content" style={{marginLeft: 20}}>
						<FormAdding onAdd={() => setAddState(false)}/>
					</div>
				</Popover>
			</div>
			<div className={classes['mb-16']}>
				<Search/>
			</div>
			<Menu/>
		</aside>
	);
};

export default Sidebar;
