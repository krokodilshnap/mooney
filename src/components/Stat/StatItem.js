import React from 'react';
import classes from '../StatDetails/stat-details-item.module.scss';
import PropTypes from 'prop-types';

const StatDetailsItem = ({income, operations}) => {
	return (
		<>
			<div className={classes.statDetailsHeading}>Последние {income ? 'доходы' : 'расходы'}</div>
			{
				operations && operations.length ?
					<>
						{operations.map(operation => (
							<div key={operation._id} className={classes.statDetailsItem}>
								<span style={{color: operation.cat.color}}>{operation.cat.name}</span>
								<span>{income ? '+' : '-' } {operation.value} ₽</span>
							</div>
						))}
					</> :
					<p>Нет операций</p>
			}
		</>
	);
};

StatDetailsItem.propTypes = {
	income: PropTypes.bool,
	operations: PropTypes.arrayOf(PropTypes.shape({
		_id: PropTypes.string,
		value: PropTypes.number,
		income: PropTypes.bool,
		check: PropTypes.shape({
			_id: PropTypes.string,
			name: PropTypes.string,
			balance: PropTypes.number
		}),
		cat: PropTypes.shape({
			_id: PropTypes.string,
			name: PropTypes.string,
			color: PropTypes.string
		})
	}))
};

export default StatDetailsItem;
