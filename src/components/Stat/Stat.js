import React, {useMemo} from 'react';
import classes from './stat.module.scss';
import ChartPie from '../ChartPie/ChartPie';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import StatItem from './StatItem';

const Stat = ({income = false}) => {
	const operationsCosts = useSelector(state => state.dashboard.operations.costs.docs);
	const operationsIncome = useSelector(state => state.dashboard.operations.income.docs);
	const operationsIncomeBalance = useSelector(state => state.dashboard.chartOperations.incomeBalance);
	const operationsCostsBalance = useSelector(state => state.dashboard.chartOperations.costsBalance);
	const chartOperationsIncome = useSelector(state => state.dashboard.chartOperations.income);
	const chartOperationsCosts = useSelector(state => state.dashboard.chartOperations.costs);
	
	const operations = useMemo(() => {
		return income ? operationsIncome : operationsCosts;
	}, [operationsCosts, operationsIncome]);
	const chartOperations = useMemo(() => {
		return income ? chartOperationsIncome : chartOperationsCosts;
	}, [chartOperationsCosts, chartOperationsIncome]);
	
	return (
		<section className={classes.stat}>
			<ChartPie balance={income ? operationsIncomeBalance : operationsCostsBalance} operations={chartOperations} income={income}/>
			<div className={classes.statDetails}>
				<StatItem operations={operations.slice(0, 5)} income={income} />
			</div>
		</section>
	);
};

Stat.propTypes = {
	income: PropTypes.bool
};

export default Stat;
