import classes from './dropdown.module.scss';
import React from 'react';

export const findChildByName = (name, children) => {
	return (
		<>
			{name === 'footer' ? <div className={classes.dropdownDelimeter}/> : null}
			{React.Children.toArray(children).find((child) => child.props.name === name)}
		</>
	);
};
