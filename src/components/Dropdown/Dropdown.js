import React, {useCallback, useRef, useState} from 'react';
import classes from './dropdown.module.scss';
import Chevron from '../../img/chevron.svg';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import {useClickOutside} from '../../hooks/useClickOutside';

const Dropdown = ({options, title, children, onChange}) => {
	const [opened, setOpened] = useState(false);
	const dropdown = useRef();
	const toggler = useRef();

	// const child = findChildByName;

	const dropdownWrapperClasses = [classes.dropdownOptionsWrapper];
	if (opened) dropdownWrapperClasses.push(classes.opened);

	const toggleOpened = () => {
		setOpened(!opened);
	};

	useClickOutside(dropdown, toggler, useCallback(toggleOpened, [opened]));

	return (
		<div className={classes.dropdown}>
			<div className={classes.dropdownControls}>
				<div
					ref={toggler}
					className={classes.dropdownControlsInput}
					onClick={toggleOpened}>
					<span>{title}</span><img src={Chevron} alt="" className={opened ? 'rotate-180' : null} />
				</div>
				{opened ? (
					<div className={dropdownWrapperClasses.join(' ')} ref={dropdown}>
						<div className={classes.dropdownOptions}>

							{options.map((option, index) => (
								<React.Fragment key={index}>
									{option.link ?
										<NavLink className={classes.dropdownOption} to={option.value}>{option.title}</NavLink> :
										<div className={classes.dropdownOption} onClick={() => onChange(index)}>{option.title}</div>
									}
								</React.Fragment>
							))}

							{children ?
								<><div className={classes.dropdownDelimeter} />{children}</> :
								null}

						</div>
					</div>
				) : null}

			</div>
		</div>
	);
};

Dropdown.propTypes = {
	options: PropTypes.arrayOf(PropTypes.shape({
		title: PropTypes.string.isRequired,
		value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
		link: PropTypes.bool
	})),
	title: PropTypes.string.isRequired,
	onChange: PropTypes.func,
	children: PropTypes.element
};


export default Dropdown;
