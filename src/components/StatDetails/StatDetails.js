import React, {useMemo} from 'react';
import StatDetailsItem from './StatDetailsItem';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import classes from './stat-details-item.module.scss';
import {Scrollbars} from 'react-custom-scrollbars';

const StatDetails = ({dates}) => {
	return (
		<div className={classes.statDetails}>
			<Scrollbars
				autoHeight
				autoHeightMin={200}
				autoHeightMax={400}>
				{dates && dates.length ?
					<>
						{dates.map(date => (
							<div key={date._id} className={classes['mb-20']}>
								<StatDetailsItem date={date}/>
							</div>
						))}
					</> : null
				}
			</Scrollbars>

		</div>
	);
};

StatDetails.propTypes = {
	income: PropTypes.bool
};

export default StatDetails;
