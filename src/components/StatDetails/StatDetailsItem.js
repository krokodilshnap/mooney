import React from 'react';
import classes from './stat-details-item.module.scss';
import PropTypes from 'prop-types';

const StatDetailsItem = ({income, date}) => {
	const statItemDate = (date) => {
		const formattedDate = new Date(date);
		const day = formattedDate.getDate();
		const year = formattedDate.getFullYear();
		const currentDay = new Date().getDate();
		const currentYear = new Date().getFullYear();

		if (day === currentDay && year === currentYear) {
			return 'Сегодня';
		} else if (day === currentDay - 1 && year === currentYear) {
			return 'Вчера';
		} else {
			return formattedDate.toLocaleString('ru-RU', {year: 'numeric', month: 'long', day: 'numeric'});
		}
	};
	return (
		<>
			<div className={classes.statDetailsHeading}>{statItemDate(date._id)}</div>
			{
				date.operations && date.operations.length ?
					<>
						{date.operations.map(operation => (
							<div key={operation._id} className={classes.statDetailsItem}>
								<span style={{color: operation.cat.color}}>{operation.cat.name}</span>
								<span>{operation.cat.income ? '+' : '-' } {operation.value} ₽</span>
							</div>
						))}
					</> :
					<p>Нет операций</p>
			}
		</>
	);
};

StatDetailsItem.propTypes = {
	income: PropTypes.bool,
	date: PropTypes.shape({
		_id: PropTypes.string,
		operations: PropTypes.arrayOf(PropTypes.shape({
			cat: PropTypes.shape({
				_id: PropTypes.string,
				name: PropTypes.string,
				color: PropTypes.string
			}),
			value: PropTypes.number
		}))
	}).isRequired
};

export default StatDetailsItem;
