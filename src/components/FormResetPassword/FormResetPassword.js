import React from 'react';
import * as Yup from 'yup';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import classes from './form-reset.module.scss';
import {useToaster} from '../../hooks/useToaster';
import {instance as axios} from '../../common/axios';
import PropTypes from 'prop-types';

const FormResetPassword = ({onSubmit}) => {
	const {showToaster} = useToaster();

	async function changePassword(values, {resetForm}) {
		await axios.post('/auth/resetPassword', {email: values.email});
		resetForm({values: ''});
		onSubmit();
		showToaster('Информация о сбросе пароля отправлена на указанный адрес электронной почты', 'success');
	}
	return (
		<div className={classes.formResetPassword}>
			<Formik
				initialValues={{email: ''}}
				validationSchema={Yup.object({
					email: Yup.string()
						.email('Некоректный E-mail')
						.required('Введите E-mail'),
				})}
				onSubmit={changePassword}
			>
				<Form>
					<div className={[classes.formGroup, classes['mb-20']].join(' ')}>
						<label htmlFor="email">E-mail:</label>
						<Field name="email" type="email" placeholder="Введите ваш E-mail" />
						<ErrorMessage name="email" component="div" className={classes.formGroupError}/>
					</div>

					<button
						type="submit"
						className={[classes.button, classes.buttonSuccess].join(' ')}>
						Сменить пароль
					</button>
				</Form>

			</Formik>
		</div>

	);
};

FormResetPassword.propTypes = {
	onSubmit: PropTypes.func
}

export default FormResetPassword;
