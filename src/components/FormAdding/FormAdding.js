import React, {useEffect, useMemo, useRef, useState} from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import '../../scss/components/datePicker.scss';
import classes from './form-adding.module.scss';
import SlideButtons from '../SlideButtons/SlideButtons';
import AppSelect from '../AppSelect/AppSelect';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import {useCats} from '../../hooks/useCats';
import {addOperation} from '../../store/actions/dashboard';
import AppDatePicker from '../AppDatePicker/AppDatePicker';
import {useTabs} from '../../hooks/useTabs';
import {useToaster} from '../../hooks/useToaster';

const FormAdding = ({onAdd}) => {
	const dispatch = useDispatch();
	const [value, setValue] = useState(0);
	const [date, setDate] = useState(new Date());
	const input = useRef();
	const {buttons, currentTab, onChangeSlideButtons} = useTabs();
	const {catsIncome, catsCosts} = useCats();
	const {checks} = useSelector((state) => state.checks);
	const cats = useMemo(() => {
		const income = currentTab === 'income';
		return income ? catsIncome : catsCosts;
	}, [currentTab]);
	const {showToaster} = useToaster();

	const [cat, setCat] = useState(cats[0]._id);
	const [check, setCheck] = useState(checks[0]._id);

	const formAddingInputClasses = [classes.formAddingInput];
	if (currentTab === 'income') formAddingInputClasses.push(classes.income);

	useEffect(() => {
		setCat(cats[0]._id);
	}, [cats]);

	const onChangeSlideButtonsHandler = (index) => {
		onChangeSlideButtons(index);
	};

	const addOperationHandler = async () => {
		await dispatch(addOperation({value, check, date, month: new Date(date).getMonth(), cat, income: currentTab === 'income'}));
		onAdd();
		showToaster(currentTab === 'income' ? 'Доход добавлен' : 'Расход добавлен', 'success', {position: 'bottom-left'});
	};

	return (
		<section className={classes.formAdding}>
			<div className={classes['mb-25']}>
				<SlideButtons buttons={buttons} onChange={(index) => onChangeSlideButtonsHandler(index)}/>
			</div>
			<div className={formAddingInputClasses.join(' ')}>
				<div>
					{currentTab === 'costs' ? '-' : '+'}
				</div>
				<input
					ref={input}
					placeholder="0"
					type="text"
					pattern="\d{0,9}"
					value={value}
					onChange={(event) => setValue(+event.target.value)}
					size={value.toString().length}/>
				<div className={classes.formAddingRuble}>₽</div>
			</div>
			<div className={[classes.formAddingDelimeter, classes['mb-25']].join(' ')}/>
			<div className={classes['mb-20']}>
				<AppSelect
					key={cats[0]._id}
					options={cats}
					getOptionLabel={(option) => option.name}
					getOptionValue={(option) => option._id}
					onChange={(value) => setCat(value._id)} />
			</div>
			<div className={classes['mb-20']}>
				<AppSelect
					key={checks[0]._id}
					options={checks}
					getOptionLabel={(option) => option.name}
					getOptionValue={(option) => option._id}
					onChange={(value) => setCheck(value._id)} />
			</div>
			<div className={classes['mb-25']}>
				<AppDatePicker onChange={(date) => setDate(date)} selected={date}/>
			</div>
			<button
				className={[classes.button, classes.buttonBlue, classes['mb-20']].join(' ')}
				onClick={addOperationHandler}>
				Добавить
			</button>
		</section>
	);
};

FormAdding.propTypes = {
	onAdd: PropTypes.func.isRequired
};

export default FormAdding;
