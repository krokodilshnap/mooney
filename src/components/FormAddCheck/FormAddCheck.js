import React from 'react';
import * as Yup from 'yup';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import classes from './form-add-check.module.scss';
import {useDispatch} from 'react-redux';
import {createCheck, editCheck} from '../../store/actions/checks';
import PropTypes from 'prop-types';
import {useToaster} from '../../hooks/useToaster';

const FormAddCheck = ({isEdit, defaultValues, onEdit}) => {
	const dispatch = useDispatch();
	const {showToaster} = useToaster();

	const saveCheckHandler = async (values, {resetForm}) => {
		await dispatch(createCheck(values));
		resetForm({values: ''});
		showToaster('Счет добавлен');
	};

	const editCheckHandler = async ({name, balance = 0}, {resetForm}) => {
		await dispatch(editCheck({name, balance, _id: defaultValues._id}));
		onEdit();
		resetForm({values: ''});
		showToaster('Счет изменен');
	};

	const formAddCheckWrapperClasses = [classes.formAddCheckWrapper];

	if (isEdit) {
		formAddCheckWrapperClasses.push(classes.formAddCheckWrapperColumn);
	}

	return (
		<div className={classes.formAddCheck}>
			<Formik
				initialValues={{name: isEdit ? defaultValues.name : '', balance: isEdit ? defaultValues.balance : ''}}
				validationSchema={Yup.object({
					name: Yup.string()
						.required('Введите название'),
				})}
				onSubmit={isEdit ? editCheckHandler : saveCheckHandler}
			>
				<Form className={formAddCheckWrapperClasses.join(' ')}>
					<div className={classes.formGroup}>
						<Field name="name" placeholder="Название счета" />
						<ErrorMessage name="name" component="div" className={classes.formGroupError}/>
					</div>
					<div className={classes.formGroup}>
						<Field name="balance" placeholder="Начальный баланс" type="number"/>
						<ErrorMessage name="balance" component="div" className={classes.formGroupError}/>
					</div>

					<button
						type="submit"
						className={[classes.button, classes.buttonSuccess].join(' ')}>
						{isEdit ? 'Сохранить' : 'Добавить'}
					</button>
				</Form>

			</Formik>
		</div>

	);
};

FormAddCheck.propTypes = {
	isEdit: PropTypes.bool,
	defaultValues: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		balance: PropTypes.number.isRequired
	}),
	onEdit: PropTypes.func
}

export default FormAddCheck;
