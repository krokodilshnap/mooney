import React from 'react';
import classes from './menu.module.scss';
import {ReactComponent as FinanceIcon} from '../../img/money.svg';
import {ReactComponent as CatIcon} from '../../img/cats.svg';
import {ReactComponent as CheckIcon} from '../../img/wallet.svg';
import {NavLink} from 'react-router-dom';

const Menu = (props) => {
	return (
		<nav className={classes.menu}>
			<NavLink to="/dashboard" className={classes.buttonInfo} activeClassName={classes.active}>
				<span>
					<FinanceIcon/>
                    Мои финансы
				</span>
			</NavLink>
			<NavLink to="checks" className={classes.buttonInfo} activeClassName={classes.active}>
				<span>
					<CheckIcon/>
                    Счета
				</span>
			</NavLink>
			<NavLink to="cats" className={classes.buttonInfo} activeClassName={classes.active}>
				<span>
					<CatIcon/>
                    Категории
				</span>
			</NavLink>
		</nav>
	);
};

export default Menu;
