import React, {forwardRef} from 'react';
import DatePicker from 'react-datepicker';
import PropTypes from 'prop-types';
import classes from './date-picker.module.scss';
import Chevron from '../../img/chevron.svg';
import ru from 'date-fns/locale/ru';

const CustomInput = forwardRef(({value, onClick}, ref) => (
	<div
		className={classes.dateInput}
		ref={ref}
		onClick={onClick}>
		<span>{value}</span><img src={Chevron} alt="" />
	</div>
));

CustomInput.displayName = 'CustomInput';

const AppDatePicker = (props) => {
	return (
		<DatePicker
			dateFormat={props.dateFormat ? props.dateFormat : 'dd.MM.yyyy'}
			locale={ru}
			{...props} customInput={<CustomInput />} />
	);
};

AppDatePicker.propTypes = {
	dateFormat: PropTypes.string
};

CustomInput.propTypes = {
	value: PropTypes.string,
	onClick: PropTypes.func
};

export default AppDatePicker;
