import React , {useState} from 'react';
import classes from './cat-item.module.scss';
import {ReactComponent as PlusIcon} from '../../img/plus.svg';
import {useDispatch} from 'react-redux';
import {deleteCatById} from '../../store/actions/cats';
import PropTypes from 'prop-types';
import {useToaster} from '../../hooks/useToaster';
import Popover from 'react-awesome-popover';
import FormConfirm from '../FormConfirm/FormConfirm';

const CatItem = ({info}) => {
	const dispatch = useDispatch();
	const {showToaster} = useToaster();
	const [deleteState, setDeleteState] = useState(false);

	function deleteCat(id) {
		dispatch(deleteCatById(id)).then(() => {
			setDeleteState(false);
			showToaster('Категория удалена');
		});
	}

	return (
		<div className={classes.catItem}>
			<span style={{color: info.color}}>{info.name}</span>
			<Popover
				open={deleteState}
				onOpen={() => setDeleteState(true)}
				onClose={() => setDeleteState(false)}
				placement="right-start"
				arrow={false}
				className={classes.popper}>
				<div className={[classes.catDelete, classes.rotate45].join(' ')} onClick={() => setDeleteState(true)}>
					<PlusIcon/>
				</div>
				<div className="popper-content" style={{marginLeft: 20}}>
					<FormConfirm onConfirm={() => deleteCat(info._id)} onDecline={() => setDeleteState(false)} />
				</div>
			</Popover>
		</div>
	);
};

CatItem.propTypes = {
	info: PropTypes.objectOf(PropTypes.string)
};

export default CatItem;
