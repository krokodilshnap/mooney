import {TOGGLE_BACKDROP} from '../actions/actionTypes';

const initialState = {
	showBackdrop: false
};

export default function globalReducer(state = initialState, action) {
	switch (action.type) {
	case TOGGLE_BACKDROP:
		return {...state, showBackdrop: !state.showBackdrop};
	default: return state;
	}
}
