import {
	CHANGE_CHART_BALANCE ,
	CHANGE_CHECK_VALUE ,
	PUSH_OPERATION ,
	SET_CHART_OPERATIONS ,
	SET_CURRENT_CHECK , SET_CURRENT_DATE ,
	SET_OPERATIONS , SET_OPERATIONS_BY_DATES
} from '../actions/actionTypes';

const initialState = {
	currentCheck: null,
	operations: {},
	currentDate: null,
	chartOperations: {},
	operationsByDates: null
};

export default function dashboardReducer(state = initialState, action) {
	switch (action.type) {
	case SET_CURRENT_CHECK:
		return {...state, currentCheck: action.check};
	case SET_OPERATIONS:
		return {...state, operations: action.operations};
	case SET_CHART_OPERATIONS:
		return {...state, chartOperations: action.operations};
	case CHANGE_CHECK_VALUE:
		return {
			...state,
			currentCheck: {
				...state.currentCheck,
				balance: action.payload.income ?
					state.currentCheck.balance + action.payload.value :
					state.currentCheck.balance - action.payload.value
			}
		};
	case PUSH_OPERATION:
		const operationStatus = action.operation.income ? 'income' : 'costs';
		const operations = {...state.operations};
		operations[operationStatus].docs.unshift(action.operation);
		return {...state, operations};
	case SET_OPERATIONS_BY_DATES:
		return {...state, operationsByDates: action.operations};
	case SET_CURRENT_DATE:
		return {
			...state,
			currentDate: {
				month: new Date(action.date).getMonth(),
				year: new Date(action.date).getFullYear()
			}
		};
	default:
		return state;
	}
}
