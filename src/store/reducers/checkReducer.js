import {CREATE_CHECK, DELETE_CHECK, EDIT_CHECK, SET_CHECKS} from '../actions/actionTypes';

const initialState = {
	checks: []
};

export default function checkReducer(state = initialState, action) {
	switch (action.type) {
	case CREATE_CHECK:
		return {...state, checks: [...state.checks, action.check]};
	case SET_CHECKS:
		return {...state, checks: action.checks};
	case EDIT_CHECK:
		const checks = [...state.checks];
		const newCheck = checks.find(check => check._id === action.values._id);
		for (const key in newCheck) {
			if (!key.startsWith('_')) {
				newCheck[key] = action.values[key];
			}
		}
		return {...state, checks};
	case DELETE_CHECK:
		return {...state, checks: state.checks.filter((check) => check._id !== action.id)};
	default:
		return state;
	}
}
