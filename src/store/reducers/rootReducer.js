import {combineReducers} from 'redux';
import authReducer from './authReducer';
import catsReducer from './catsReducer';
import globalReducer from './globalReducer';
import checkReducer from './checkReducer';
import dashboardReducer from './dashboardReducer';
import searchReducer from './searchReducer';

export default combineReducers({
	auth: authReducer,
	cats: catsReducer,
	checks: checkReducer,
	dashboard: dashboardReducer,
	search: searchReducer,
	global: globalReducer
});
