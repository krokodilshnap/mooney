import {AUTH_SUCCESS, LOGOUT_SUCCESS} from '../actions/actionTypes';

const initialState = {
	user: ''
};

export default function(state = initialState, action) {
	switch (action.type) {
	case AUTH_SUCCESS:
		return {...state, user: action.user};
	case LOGOUT_SUCCESS:
		return {...state, user: ''};
	default:
		return state;
	}
}
