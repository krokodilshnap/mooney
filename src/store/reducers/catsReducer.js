import {CREATE_CAT, DELETE_CAT, SET_CATS} from '../actions/actionTypes';

const initialState = {
	cats: []
};

export default function(state = initialState, action) {
	switch (action.type) {
	case CREATE_CAT:
		return {...state, cats: [...state.cats, action.cat]};
	case SET_CATS:
		return {...state, cats: action.cats};
	case DELETE_CAT:
		const cats = state.cats;
		const targetIndex = cats.findIndex((cat) => cat._id === action.id);
		cats.splice(targetIndex, 1);
		return {...state, cats};
	default:
		return state;
	}
}
