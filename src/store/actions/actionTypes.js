// auth
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
// cats
export const CREATE_CAT = 'CREATE_CAT';
export const SET_CATS = 'SET_CATS';
export const DELETE_CAT = 'DELETE_CAT';
// backdrop
export const TOGGLE_BACKDROP = 'TOGGLE_BACKDROP';
// checks
export const CREATE_CHECK = 'CREATE_CHECK';
export const SET_CHECKS = 'SET_CHECKS';
export const DELETE_CHECK = 'DELETE_CHECK';
export const EDIT_CHECK = 'EDIT_CHECK';
export const SET_CURRENT_CHECK = 'SET_CURRENT_CHECK';
// dashboard
export const SET_OPERATIONS = 'SET_OPERATIONS';
export const SET_CHART_OPERATIONS = 'SET_CHART_OPERATIONS';
export const CHANGE_CHECK_VALUE = 'CHANGE_CHECK_VALUE';
export const PUSH_OPERATION = 'PUSH_OPERATION';
export const SET_OPERATIONS_BY_DATES = 'SET_OPERATIONS_BY_DATES';
export const SET_CURRENT_DATE = 'SET_CURRENT_DATE';
