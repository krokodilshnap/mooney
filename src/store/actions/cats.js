import {CREATE_CAT, DELETE_CAT, SET_CATS} from './actionTypes';
import {instance as axios} from '../../common/axios';

export function createCat(values) {
	return async (dispatch) => {
		const response = await axios.post('/cats', {...values});
		dispatch(createCatSuccess(response.data));
	};
}

export function getCats() {
	return async (dispatch) => {
		const response = await axios.get('/cats');
		dispatch(getCatsSuccess(response.data));
	};
}

export function deleteCatById(id) {
	return async (dispatch) => {
		await axios.delete(`/cats/${id}`);
		dispatch(deleteCatSuccess(id));
	};
}

function createCatSuccess(cat) {
	return {
		type: CREATE_CAT,
		cat
	};
}

function getCatsSuccess(cats) {
	return {
		type: SET_CATS,
		cats
	};
}

function deleteCatSuccess(id) {
	return {
		type: DELETE_CAT,
		id
	};
}

