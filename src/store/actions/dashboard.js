import {instance as axios} from '../../common/axios';
import {
	CHANGE_CHECK_VALUE ,
	PUSH_OPERATION ,
	SET_CHART_OPERATIONS ,
	SET_CURRENT_CHECK , SET_CURRENT_DATE ,
	SET_OPERATIONS , SET_OPERATIONS_BY_DATES
} from './actionTypes';

export function addOperation(operation) {
	return async (dispatch, getState) => {
		axios.post('/operations', operation).then((response) => {
			const operation = response.data;
			const {currentCheck, currentDate} = getState().dashboard;
			dispatch(changeCheckValue(operation.income, operation.value));
			const operationDate = {
				month: new Date(response.data.date).getMonth(),
				year: new Date(response.data.date).getFullYear()
			};
			const isCurrentDate = currentDate.month === operationDate.month && currentDate.year === operationDate.year;
			if (currentCheck._id === response.data.check._id && isCurrentDate) {
				dispatch(pushOperation(operation));
				dispatch(getChartOperations(currentCheck._id, operation.date));
			}
		});
	};
}

export function setCurrentDate(date) {
	return {
		type: SET_CURRENT_DATE,
		date
	};
}

export function getOperations(checkId, params) {
	return async dispatch => {
		const response = await axios.get(`/operations/by/check/${checkId}`, {params});
		dispatch(setOperations(response.data));
	};
}

export function getOperationsByAllDates(checkId, params) {
	return async dispatch => {
		const response = await axios.get(`/operations/by/dates/${checkId}`, {params});
		dispatch(setOperationsByDates(response.data));
	};
}

export function getChartOperations(check, date) {
	return async dispatch => {
		const response = await axios.get('/operations/chart', {params: {check, month: new Date(date).getMonth()}});
		dispatch(setChartOperations(response.data));
	};
}

export function setChartOperations(operations) {
	return {
		type: SET_CHART_OPERATIONS,
		operations
	};
}

export function setOperationsByDates(operations) {
	return {
		type: SET_OPERATIONS_BY_DATES,
		operations
	};
}

export function pushOperation(operation) {
	return {
		type: PUSH_OPERATION,
		operation
	};
}

export function changeCheckValue(income, value) {
	return {
		type: CHANGE_CHECK_VALUE,
		payload: {
			income, value
		}
	};
}

export function setOperations(operations) {
	return {
		type: SET_OPERATIONS,
		operations
	};
}

export function getCurrentCheck(id) {
	return async (dispatch) => {
		const response = await axios.get(`/checks/${id}`);
		dispatch(setCurrentCheck(response.data));
	};
}

export function setCurrentCheck(check) {
	return {
		type: SET_CURRENT_CHECK,
		check
	};
}
