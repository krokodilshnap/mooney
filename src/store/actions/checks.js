import {instance as axios} from '../../common/axios';
import {CREATE_CHECK, DELETE_CHECK, EDIT_CHECK, SET_CHECKS} from './actionTypes';

export function createCheck(values) {
	return async (dispatch) => {
		const checkResponse = await axios.post('/checks', values);
		dispatch(createCheckSuccess(checkResponse.data));
	};
}

export function getChecks() {
	return async (dispatch) => {
		const checksResponse = await axios.get('/checks');
		dispatch(getChecksSuccess(checksResponse.data));
	};
}

export function editCheck(values) {
	return async (dispatch) => {
		const response = await axios.patch(`/checks/${values._id}`, values);
		dispatch(editCheckSuccess(response.data));
	};
}

export function deleteCheck(id) {
	return async (dispatch) => {
		await axios.delete(`/checks/${id}`);
		dispatch(deleteChecksSuccess(id));
	};
}

export function editCheckSuccess(values) {
	return {
		type: EDIT_CHECK,
		values
	};
}

export function deleteChecksSuccess(id) {
	return {
		type: DELETE_CHECK,
		id
	};
}

export function createCheckSuccess(check) {
	return {
		type: CREATE_CHECK,
		check
	};
}

export function getChecksSuccess(checks) {
	return {
		type: SET_CHECKS,
		checks
	};
}
