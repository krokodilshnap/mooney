import {instance as axios} from '../../common/axios';
import {AUTH_SUCCESS, LOGOUT_SUCCESS} from './actionTypes';

export function auth(password, email) {
	return async (dispatch) => {
		try {
			const response = await axios.post('/auth/login', {password, email});
			dispatch(authSuccess(response.data));
		} catch (e) {
			console.log(e, 'error');
			if (e) throw e;
		}
	};
}

export function logoutUser() {
	return async (dispatch) => {
		try {
			await axios.get('/auth/logout');
			dispatch(logoutSuccess());
		} catch (e) {
			console.log(e);
		}
	};
}

export function logoutSuccess() {
	return {
		type: LOGOUT_SUCCESS
	};
}

export function authSuccess(user) {
	return {
		type: AUTH_SUCCESS,
		user
	};
}

export function autoLogin() {
	return async (dispatch) => {
		try {
			const response = await axios.get('/auth/me');
			dispatch(authSuccess(response.data));
		} catch (e) {
			console.log(e);
		}
	};
}
