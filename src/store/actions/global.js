import {TOGGLE_BACKDROP} from './actionTypes';

export function toggleBackdrop() {
	return {
		type: TOGGLE_BACKDROP
	};
}
