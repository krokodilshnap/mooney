import React from 'react';
import classes from './settings.module.scss';
import {useSelector} from 'react-redux';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';
import {instance as axios} from '../../../common/axios';
import {useToaster} from '../../../hooks/useToaster';

const Settings = (props) => {
	const user = useSelector((state) => state.auth.user);
	const {showToaster} = useToaster();

	const saveSettings = async (values, {resetForm}) => {
		await axios.patch('/auth', values);
		resetForm({values: ''});
		showToaster('Пароль успешно изменен');
	};

	return (
		<section className={classes.settingsPage}>
			<h1 className={classes.heading}>
				{user.email}
			</h1>
			{user.googleId ? 
				'Выполнен вход через Google' : 
				user.vkId ? 
					'Выполнен вход через Вконтакте' : 
					<Formik
						initialValues={{password: '', new: '', confirm: ''}}
						validationSchema={Yup.object({
							password: Yup.string()
								.min(6, 'Пароль должен быть не менее 6 символов')
								.required('Введите пароль'),
							new: Yup.string()
								.min(6, 'Пароль должен быть не менее 6 символов')
								.required('Введите пароль'),
							confirm: Yup.string()
								.oneOf([Yup.ref('new'), null], 'Пароли не совпадают')
						})}
						onSubmit={saveSettings}
					>
						<Form>
							<div className={classes.changePasswordForm}>
								<div className={classes.formGroup}>
									<Field name="password" type="password" placeholder="Текущий пароль" />
									<ErrorMessage name="password" component="div" className={classes.formGroupError}/>
								</div>
								<div className={classes.formGroup}>
									<Field name="new" type="password" placeholder="Новый пароль" />
									<ErrorMessage name="new" component="div" className={classes.formGroupError}/>
								</div>
								<div className={classes.formGroup}>
									<Field name="confirm" type="password" placeholder="Повторите новый пароль" />
									<ErrorMessage name="confirm" component="div" className={classes.formGroupError}/>
								</div>
								<button type="submit" className={[classes.button, classes.buttonSuccess].join(' ')}>Сохранить</button>
							</div>
						</Form>
					</Formik>}
			

		</section>
	);
};

export default Settings;
