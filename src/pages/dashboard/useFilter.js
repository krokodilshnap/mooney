import {
	getChartOperations ,
	getCurrentCheck ,
	getOperations ,
	getOperationsByAllDates , setCurrentDate
} from '../../store/actions/dashboard';
import {useEffect , useState} from 'react';
import {useDispatch , useSelector} from 'react-redux';

export function useFilter() {
	const [date, setDate] = useState(new Date());
	const [showDetails, setShowDetails] = useState(false);
	const dispatch = useDispatch();
	const {checks} = useSelector((state) => state.checks);
	const [loaded, setLoaded] = useState(false);
	const {currentCheck} = useSelector(state => state.dashboard);

	useEffect(() => {
		if (checks.length) {
			Promise.all([
				dispatch(getCurrentCheck(checks[0]._id)),
				getOperationsInfo(checks[0]._id, date)
			]).then(() => {
				dispatch(setCurrentDate(date));
				setLoaded(true);
			});
		}
	}, [checks]);

	async function onChangeDate(date) {
		setDate(date);
		setShowDetails(false);
		dispatch(setCurrentDate(date));
		return getOperationsInfo(checks[0]._id, date);
	}

	function getOperationsInfo(checkId, date) {
		return Promise.all([
			getOperationsByDate(checkId, {date}),
			dispatch(getChartOperations(checkId, date))
		]);
	}

	async function onChangeCheck(check) {
		setShowDetails(false);
		await dispatch(getCurrentCheck(check._id));
		await getOperationsInfo(check._id, date);
	}

	function getOperationsByDate(checkId, params) {
		const {limit, offset, date} = params;
		return dispatch(getOperations(checkId, {month: new Date(date).getMonth(), limit, offset}));
	}

	function loadMoreOperations() {
		setShowDetails(curValue => {
			if (!curValue) {
				dispatch(getOperationsByAllDates(currentCheck._id, {month: new Date(date).getMonth()}));
			}

			return !curValue;
		});
	}

	return {
		onChangeCheck,
		onChangeDate,
		loaded,
		showDetails,
		date,
		checks,
		loadMoreOperations
	};
}
