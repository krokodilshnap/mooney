import React , {useEffect , useMemo} from 'react';
import classes from './dashboard.module.scss';
import Stat from '../../components/Stat/Stat';
import StatDetails from '../../components/StatDetails/StatDetails';
import {ReactComponent as ChevronIcon} from '../../img/chevron-thin.svg';
import AppSelect from '../../components/AppSelect/AppSelect';
import {useSelector} from 'react-redux';
import AppDatePicker from '../../components/AppDatePicker/AppDatePicker';
import {useFilter} from './useFilter';
import Loader from '../../components/Loader/Loader';
import {instance as axios} from '../../common/axios';
import {useToaster} from '../../hooks/useToaster';
import {useHistory} from 'react-router-dom';

const Dashboard = ({location}) => {
	const {currentCheck} = useSelector(state => state.dashboard);
	const {balancePerPeriod} = useSelector(state => state.dashboard.chartOperations);
	const {income, costs} = useSelector(state => state.dashboard.chartOperations);
	const {operationsByDates} = useSelector(state => state.dashboard);
	const {user} = useSelector(state => state.auth);
	const {showToaster} = useToaster();
	const history = useHistory();
	const operationsCount = useMemo(() => {
		if (income && costs) {
			return {
				income: income.length || 0,
				costs: costs.length || 0
			};
		}
	}, [costs, income]);

	const {
		onChangeCheck,
		onChangeDate,
		loaded,
		showDetails,
		date,
		checks,
		loadMoreOperations
	} = useFilter();

	useEffect(() => {
		const uid = new URLSearchParams(location.search).get('uid');
		if (uid) {
			if (!user.isConfirmed) {
				axios.post('/auth/confirm', {uid}).then(() => {
					window.location.reload();
				}).catch(e => {
					if (e.response.errors) {
						showToaster(e.response.errors[0].msg, 'error');
					}
				});
			}
		}
	}, []);

	const moreClasses = [classes.dashboardMore];
	if (showDetails) moreClasses.push(classes.opened);

	const balance = (value) => {
		return (value > 0 ? `+` : ``) + `${value.toLocaleString()}  ₽`;
	};

	return (
		<section className={classes.dashboard}>
			{loaded ? <>
				<header className={classes.dashboardHeader}>
					<div className={classes.dashboardBalance}>
						<div className={classes.dashboardBalanceItem}>
							<div>Текущий остаток</div>
							<div>{balance(currentCheck.balance)}</div>
						</div>
						<div className={classes.dashboardBalanceItem}>
							<div>Баланс за период</div>
							<div>{balance(balancePerPeriod)}</div>
						</div>
					</div>
					<div className={classes.dashboardControls}>
						<div>
							<AppDatePicker
								showMonthYearPicker
								showFullMonthYearPicker
								selected={date}
								dateFormat="MMM yyyy"
								onChange={onChangeDate}
							/>
						</div>
						<div>
							<AppSelect
								options={checks}
								onChange={onChangeCheck}
								getOptionLabel={(option) => option.name}
								getOptionValue={(option) => option._id}/>
						</div>
					</div>
				</header>
				<div className={classes.dashboardChartsWrapper}>
					<div className={classes.dashboardCharts}>
						<Stat />
						<Stat income/>
					</div>
					{showDetails ?
						<>
							{operationsByDates ? <div className={classes.dashboardCharts}>
								<StatDetails dates={operationsByDates.costs}/>
								<StatDetails dates={operationsByDates.income}/>
							</div> : <Loader/>}
						</> :
						null}
					{operationsCount.income || operationsCount.costs ? 
						<div className={moreClasses.join(' ')} onClick={loadMoreOperations}>
							{showDetails ? <ChevronIcon/> : (
								<>
									<div className={classes.dashboardMoreHeading}>Подробнее</div>
									<span/><span/><span/>
								</>
							)}
						</div> : null}
				</div>
			</> :
				<Loader/>}
		</section>
	);
};

export default Dashboard;
