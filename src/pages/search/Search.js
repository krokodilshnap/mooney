import React , {useEffect , useState} from 'react';
import classes from './search.module.scss';
import {useHistory} from 'react-router-dom';
import {instance as axios} from '../../common/axios';
import StatDetails from '../../components/StatDetails/StatDetails';

const Search = () => {
	const history = useHistory();
	const query = history.location.search;
	const [operations, setOperations] = useState();

	useEffect(() => {
		axios.get(`/search/${query}`, {params: {limit: 40, offset: 0}}).then(response => {
			setOperations(response.data);
		});
	}, [query]);
	return (
		<section className={classes.searchSection}>
			<h1 className={classes.heading}>
				Результаты поиска
			</h1>
			<div className={classes.searchSectionResults}>
				{operations && operations.length ? <StatDetails dates={operations}/> : <p>Ничего не найдено</p>}
			</div>
		</section>
	);
};

export default Search;