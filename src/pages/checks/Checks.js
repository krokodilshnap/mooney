import React from 'react';
import classes from './checks.module.scss';
import FormAddCheck from '../../components/FormAddCheck/FormAddCheck';
import CheckItem from '../../components/CheckItem/CheckItem';
import useChecks from '../../hooks/useChecks';

const Checks = (props) => {
	const {checks} = useChecks();

	return (
		<section className={classes.checksSection}>
			<h1 className={classes.heading}>
				Мои счета
			</h1>

			<div className={classes['mb-25']}>
				<FormAddCheck/>
			</div>

			{checks.map((check) => (
				<div key={check._id} className={classes['mb-20']}>
					<CheckItem info={check}/>
				</div>
			))}
		</section>
	);
};

export default Checks;
