import React, {useEffect, useState} from 'react';
import classes from './login.module.scss';
import Logo from '../../components/Logo/Logo';
import loginImg from '../../img/login-img.svg';
import SlideButtons from '../../components/SlideButtons/SlideButtons';
import LoginForm from '../../components/LoginForm/LoginForm';
import RegisterForm from '../../components/RegisterForm/RegisterForm';
import {instance as axios} from '../../common/axios';

const Login = (props) => {
	const [buttons, setButtons] = useState([
		{
			title: 'Вход',
			value: 'enter',
			active: true
		},
		{
			title: 'Регистрация',
			value: 'register',
			active: false
		}
	]);
	const [currentTab, setCurrentTab] = useState('enter');

	useEffect(() => {
		axios.get('/auth/me').then((response) => {
			console.log(response);
		});
	}, []);

	function onChangeSlideButtons(index) {
		const newButtons = [...buttons];
		newButtons.forEach((button) => button.active = false);
		newButtons[index].active = true;
		setCurrentTab(newButtons[index].value);
		setButtons(newButtons);
	}

	return (
		<div className={classes.loginWrap}>
			<img src={loginImg} className={classes.loginImg} alt=""/>
			<section>
				<div className={classes['mb-35']}>
					<Logo big />
				</div>
				<div className={classes['mb-35']}>
					<SlideButtons buttons={buttons} onChange={onChangeSlideButtons}/>
				</div>
				{ currentTab === 'enter' ? <LoginForm/> : <RegisterForm/> }
			</section>
		</div>
	);
};

export default Login;
