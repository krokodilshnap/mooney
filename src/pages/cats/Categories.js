import React from 'react';
import classes from './categories.module.scss';
import CatItem from '../../components/CatItem/CatItem';
import * as Yup from 'yup';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import {useDispatch} from 'react-redux';
import {createCat} from '../../store/actions/cats';
import ColorPicker from '../../components/ColorPicker/ColorPicker';
import {useCats} from '../../hooks/useCats';
import {useColorPicker} from './useColorPicker';
import {useToaster} from '../../hooks/useToaster';

const Categories = (props) => {
	const dispatch = useDispatch();
	const {catsCosts, catsIncome} = useCats();
	const {handleChangeColor, color, colors} = useColorPicker();
	const {showToaster} = useToaster();

	function saveCat(values, {resetForm}) {
		dispatch(createCat({...values, color})).then(() => {
			resetForm({values: ''});
			showToaster('Категория добавлена');
		});
	}


	return (
		<section className={classes.catsSection}>
			<div className={classes.catsList}>
				<h1 className={classes.heading}>
                    Расходы
				</h1>
				<>
					{ catsCosts.length ? catsCosts.map((cat) => (
						<>
							{ !cat.noCat ? <div className="catsItemWrapper" key={cat._id}>
								<CatItem info={cat}/>
							</div> : null }
						</>
					)) : <p className={classes.catsListEmpty}>Нет категорий</p> }
				</>
			</div>
			<div className={classes.catsList}>
				<h1 className={classes.heading}>
                    Доходы
				</h1>
				<>
					{catsIncome.length ? catsIncome.map((cat) => (
						<div className="catsItemWrapper" key={cat._id}>
							<CatItem info={cat}/>
						</div>
					)) : <p className={classes.catsListEmpty}>Нет категорий</p> }
				</>
			</div>
			<div className={classes.catsCreate}>
				<h1 className={classes.heading}>
                    Добавить новую категорию
				</h1>

				<Formik
					initialValues={{name: '', income: 'false'}}
					validationSchema={Yup.object({
						name: Yup.string()
							.required('Введите название'),
					})}
					onSubmit={saveCat}
				>

					<Form>
						<div className={classes.catsCreateFormGroup}>
							<div className={classes.formGroup}>
								<Field name="name" placeholder="Название категории" />
								<ErrorMessage name="name" component="div" className={classes.formGroupError}/>
							</div>
						</div>

						<div className={classes.catsCreateControls}>
							<div className={[classes.formGroup, classes.formGroupRadio].join(' ')}>
								<Field name="income" type="radio" value="false" id="radio" />
								<label htmlFor="radio">
									<span>Расход</span>
								</label>
							</div>
							<div className={[classes.formGroup, classes.formGroupRadio].join(' ')}>
								<Field name="income" type="radio" value="true" id="radio2" />
								<label htmlFor="radio2">
									<span>Доход</span>
								</label>
							</div>
						</div>
						<div className={classes['mb-20']}>
							<ColorPicker colors={colors.current} onChange={handleChangeColor}/>
						</div>
						<button type="submit" className={[classes.button, classes.buttonSuccess].join(' ')}>Добавить</button>
					</Form>

				</Formik>
			</div>
		</section>
	);
};

export default Categories;
