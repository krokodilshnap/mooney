import cogoToast from 'cogo-toast';

export function useToaster() {
	function showToaster(message, type = 'success', params) {
		console.log('showToaster');
		return cogoToast[type](message, params);
	}

	return {
		showToaster
	};
}
