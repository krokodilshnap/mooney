import {useEffect} from 'react';

export function useClickOutside(ref, toggler, handler) {
	useEffect(() => {
		const listener = (event) => {
			if (!ref.current || toggler.current.contains(event.target)) {
				return false;
			}

			handler();
		};

		document.addEventListener('click', listener);
		document.addEventListener('touchstart', listener);

		return () => {
			document.removeEventListener('click', listener);
			document.removeEventListener('touchstart', listener);
		};
	}, [ref, handler, toggler]);
}
