import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {getChecks} from '../store/actions/checks';

export default function useChecks() {
	const dispatch = useDispatch();
	const {checks} = useSelector((state) => state.checks);

	useEffect(() => {
		dispatch(getChecks());
		// eslint-disable-next-line
	}, []);

	return {checks};
}
