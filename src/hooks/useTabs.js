import {useState} from 'react';

export function useTabs() {
	const [currentTab, setCurrentTab] = useState('costs');
	const [buttons, setButtons] = useState([
		{
			title: 'Расход',
			value: 'costs',
			active: true
		},
		{
			title: 'Доход',
			value: 'income',
			active: false
		}
	]);

	const onChangeSlideButtons = (tabIndex) => {
		const newButtons = [...buttons];
		newButtons.forEach((button) => button.active = false);
		newButtons[tabIndex].active = true;
		setCurrentTab(newButtons[tabIndex].value);
		setButtons(newButtons);
	};

	return {buttons, currentTab, onChangeSlideButtons};
}

