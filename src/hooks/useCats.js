import {useDispatch, useSelector} from 'react-redux';
import {getCats} from '../store/actions/cats';
import {useEffect} from 'react';

export function useCats() {
	const dispatch = useDispatch();
	const {cats} = useSelector((state) => state.cats);
	const catsCosts = cats.filter((cat) => !cat.income);
	const catsIncome = cats.filter((cat) => cat.income);

	useEffect(() => {
		dispatch(getCats());
		// eslint-disable-next-line
	}, []);

	return {cats, catsCosts, catsIncome};
}
